function [Rlogs, pbs, nb_falsified] = run_breach(B, R, n_run)
% run_breach runs 50 instances of Global Nelder Mead with different seeds

if nargin<=2
    n_run = 50;
end

for i=1:numel(R)
    nb_falsified = 0;
    j=0;
    fprintf('Solving for %s ... \n',R(i).req_monitors{1}.name);
    for j = 1:n_run
        pbs(i,j) = FalsificationProblem(B, R(i));        
        pbs(i,j).solver_options.nb_max_corners = 64;
        pbs(i,j).solver_options.nb_new_trials = 100;
        pbs(i,j).solver_options.quasi_rand_seed = 300*(j-1)+1;        
        pbs(i,j).max_obj_eval = 300;
        pbs(i,j).verbose = 0;
        if isa(B,'BreachSimulinkSystem')
            pbs(i,j).SetupDiskCaching();
        end
        pbs(i,j).display='off';
        fprintf('     %g. ', j);
        pbs(i,j).solve();
        Rlogs(i,j) = pbs(i,j).GetLog();
        if any(pbs(i,j).obj_log<0)
            fprintf('Falsified after %g simulations.', find(pbs(i,j).obj_log<0, 1));
            nb_falsified = nb_falsified+1;
        else
            fprintf('Not falsified. Best robustness: %g',  pbs(i,j).obj_best);
        end
        fprintf('\n');
    end
    fprintf('\n');
end

end
