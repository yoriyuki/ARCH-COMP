Software Requirements
---------------------
- Mathematica 11.3 (ensure that license is activated)
- Perl

Package Contents
----------------
1. Advanced/: The advanced benchmark suite, one file per example
2. Basic/: The basic benchmark suite, one file per example
3. Nonlinear/: The nonlinear benchmark suite, one file per example
4. index/advanced.txt: Lists advanced suite benchmark examples with timeout (maximum allowed duration)
   index/basic.txt: Lists basic suite benchmark examples with timeout (maximum allowed duration)
   index/nonlinear.txt: Lists nonlinear suite benchmark examples with timeout (maximum allowed duration)
   index/headerDL-lazy-auto.txt: KeYmaera 3 automation configuration used in the benchmark
   index/headerDL.txt: KeYmaera 3 automation configuration (forwards to headerDL-lazy-auto.txt)
5. KeYmaera_3.6.17: The KeYmaera 3 theorem prover and required libraries
6. results/: The benchmark competition results as reported in the competition report
6. runDLProofs.pl: Runs the benchmark
7. runKeYmaera: Helper script to start KeYmaera 3 (used by runDLProofs.pl)

Installation
------------
1. Run install.sh to download KeYmaera 3
2. Adapt line 2 in "runKeYmaera" (PATH=${PATH}:"/Applications/Mathematica.app") to fit your Mathematica setup

Running the Benchmark
---------------------
1. Run the script "runDLProofs.pl".
   On first execution, a dialog box will ask for the Mathematica path. 
   Tick the checkbox "Always use these settings as defaults".
   Confirm settings with "Apply" in Mathematica Properties and "Apply" on the overall dialog. 
   Close the dialog by clicking "Ok".
   A configuration warning dialog will show. Close it by clicking "Yes".

Troubleshooting
---------------
1. The script "runDLProofs.pl" was tested on MacOS. 
   When running on Linux, comment out the MacOS killtree subroutine and uncomment the Linux killtree subroutine.