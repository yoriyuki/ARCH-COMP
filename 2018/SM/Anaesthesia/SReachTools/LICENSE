This stochastic reachability toolbox (SReachTools), its source, and its
documentation are copyright © 2018 by Abraham P. Vinod and Joseph D. Gleason.

Use or creating copies of all or part of this work is subject to the following
licensing agreement.

This license is derived from 
1) the ACM Software Copyright and License Agreement (1998), which can be found
at:

    http://www.acm.org/pubs/copyright_policy/softwareCRnotice.html

2) the license of Toolbox of Level Set Methods owned by Ian M. Mitchell.

LICENSE

The stochastic reachability toolbox (SReachTools), its source, and its
documentation (hereafter, Software) is copyrighted by Abraham P. Vinod and
Joseph D. Gleason (hereafter, Developers) and ownership of all rights, title and
interest in and to the Software remains with the Developers. By using or copying
the Software, you (hereafter, the User) agree to abide by the terms of this
Agreement.

NONCOMMERICIAL USE

The Developers grant the User a royalty-free, nonexclusive right to execute,
copy, modify and distribute the Software solely for academic, research and other
similar noncommercial uses, subject to the following conditions:

1. The User acknowledges that the Software is still in the development
   stage and that it is being supplied "as is," without any support
   services from the Developers.  NEITHER THE DEVELOPERS NOR THEIR
   EMPLOYERS MAKE ANY REPRESENTATIONS OR WARRANTIES, EXPRESS OR
   IMPLIED, INCLUDING, WITHOUT LIMITATION, ANY REPRESENTATIONS OR
   WARRANTIES OF THE MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR
   PURPOSE, OR THAT THE APPLICATION OF THE SOFTWARE, WILL NOT INFRINGE
   ON ANY PATENTS OR OTHER PROPRIETARY RIGHTS OF OTHERS.

2. The Developers and their employers shall not be held liable for
   direct, indirect, special, incidental or consequential damages
   arising from any claim by the User or any third party with respect
   to uses allowed under this Agreement, or from any use of the
   Software, even if the Developers or their employers have been advised
   of the possibility of such damage.

3. The User agrees to fully indemnify and hold harmless the Developers
   and their employers from and against any and all claims, demands,
   suits, losses, damages, costs and expenses arising out of the
   User's use of the Software, including, without limitation, arising
   out of the User's modification of the Software.

4. The User may modify the Software and distribute that modified work
   to third parties provided that: (a) if posted separately, it
   clearly acknowledges that it contains material copyrighted by the
   Developers (b) no charge is associated with such copies, (c) User
   agrees to notify the Developers of the distribution, and (d) User
   clearly notifies secondary users that such modified work is not the
   original Software.

5. Any distribution of all or part of the Software or modified
   versions must contain the above copyright notice and this license.

6. This agreement will terminate immediately upon the User's breach
   of, or non-compliance with, any of its terms. The User may be held
   liable for any copyright infringement or the infringement of any
   other proprietary rights in the Software that is caused or
   facilitated by the User's failure to abide by the terms of this
   agreement.

7. This agreement will be construed and enforced in accordance with the law of
   the New Mexico applicable to contracts performed entirely within that
   Province. The parties irrevocably consent to the exclusive jurisdiction of
   the provincial or federal courts located in the City of Albuquerque for all
   disputes concerning this agreement.

COMMERCIAL or OTHER USE

Any User wishing to make a commercial or other use of the Software is
encouraged to contact the Developer at aby.vinod@gmail.com to arrange
an appropriate license. Commercial use includes (1) integrating or
incorporating all or part of the source code into a product for sale
or license by, or on behalf of, the User to third parties, or (2)
distribution of a compiled or source code version of the Software to
third parties for use with a commercial product sold or licensed by,
or on behalf of, the User.
