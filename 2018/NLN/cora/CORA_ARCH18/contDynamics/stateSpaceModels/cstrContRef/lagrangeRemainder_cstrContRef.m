function lf=lagrangeRemainder_cstrContRef(x,u,dx,du,p)

lf=interval();

lf(1,1)=dx(2)*((dx(2)*((1260000000000000*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(x(2) + 350)^3 - (5512500000000000000*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(x(2) + 350)^4))/2 - (315000000000000*dx(1)*exp(-8750/(x(2) + 350)))/(x(2) + 350)^2) - (315000000000000*dx(1)*dx(2)*exp(-8750/(x(2) + 350)))/(x(2) + 350)^2;
lf(2,1)=(33740585774058576875*dx(1)*dx(2)*exp(-8750/(x(2) + 350)))/(512*(x(2) + 350)^2) - dx(2)*((dx(2)*((33740585774058576875*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(128*(x(2) + 350)^3) - (147615062761506273828125*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(128*(x(2) + 350)^4)))/2 - (33740585774058576875*dx(1)*exp(-8750/(x(2) + 350)))/(512*(x(2) + 350)^2));
lf(3,1)=dx(4)*((dx(4)*((1260000000000000*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(x(4) + 350)^3 - (5512500000000000000*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(x(4) + 350)^4))/2 - (315000000000000*dx(3)*exp(-8750/(x(4) + 350)))/(x(4) + 350)^2) - (315000000000000*dx(3)*dx(4)*exp(-8750/(x(4) + 350)))/(x(4) + 350)^2;
lf(4,1)=(33740585774058576875*dx(3)*dx(4)*exp(-8750/(x(4) + 350)))/(512*(x(4) + 350)^2) - dx(4)*((dx(4)*((33740585774058576875*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(128*(x(4) + 350)^3) - (147615062761506273828125*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(128*(x(4) + 350)^4)))/2 - (33740585774058576875*dx(3)*exp(-8750/(x(4) + 350)))/(512*(x(4) + 350)^2));
